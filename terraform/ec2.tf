resource "aws_instance" "ec2" {
  ami           = "${var.ami}"
  instance_type = "${var.type}"
  key_name	= "terraform"
  availability_zone = "${var.zone}"
  subnet_id = "${var.subnet}"
  vpc_security_group_ids = ["${aws_security_group.terraform-sg.id}"]
  user_data = "${file("install.sh")}" 

  tags = {
    Name = "${var.product}-${var.environment}"
  }
}
