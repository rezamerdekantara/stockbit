#! /bin/bash

sudo apt-get -y update
sudo apt-get install -y apt-transport-https ca-certificates curl software-properties-common
sudo apt-get install -y docker.io=20.10.7-0ubuntu5~18.04.3
sudo curl -L "https://github.com/docker/compose/releases/download/1.27.4/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose
sudo chmod +x /usr/bin/docker-compose
