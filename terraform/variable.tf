variable "product" {}
variable "environment" {}
variable "ami" {}
variable "type" {}
variable "zone" {}
variable "subnet" {}
variable "vpcid" {}
